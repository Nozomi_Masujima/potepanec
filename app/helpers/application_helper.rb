module ApplicationHelper
  STORE_NAME = 'BIGBAG Store'.freeze
  def full_title(title)
    if title.present?
      "#{title} - #{STORE_NAME}"
    else
      STORE_NAME
    end
  end
end
