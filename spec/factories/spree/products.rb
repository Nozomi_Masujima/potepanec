FactoryBot.define do
  factory :spree_product, class: 'Spree::Product' do
    id { 1 }
    name { 'NoMethodTshirt' }
    description { 'This Tshirt is error. It is Major Error.This error happen too many.Rails aborted.' }
    slug { 'no_method_tshirt' }
    tax_category_id { 1 }
    shipping_category_id { 1 }
    price { 0.3399e2 }
  end
end
