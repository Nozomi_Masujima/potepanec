require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe '正常' do
    let!(:spree_product) { FactoryBot.create(:spree_product) }

    it "商品個別ページの表示" do
      get potepan_product_path(spree_product.id)
      expect(response).to have_http_status(200)
    end
  end
end
