require 'rails_helper'

RSpec.describe ApplicationHelper do
  include ApplicationHelper
  describe "タイトルの表示" do
    context 'titleがnilのとき' do
      it { expect(full_title(nil)).to eq(STORE_NAME) }
    end

    context 'titleが空欄のとき' do
      let(:title) { "" }

      it { expect(full_title('')).to eq(STORE_NAME) }
    end

    context 'titleが空欄でないとき' do
      let(:title) { "テストページタイトル" }

      it { expect(full_title(title)).to eq("テストページタイトル - #{STORE_NAME}") }
    end
  end
end
